# Reasoning Reading Group


We will reboot our periodic meetings by starting on **Thursday** Nov 17th and meeting **every two weeks from 5pm to 6pm in room G.03 at Informatics Forum**. The discussion will then continue at a pub!

As in the previous season, we will explore all possible aspects of reasoning: from *automated reasoning* to *human reasoning*, connecting works from *probabilistic reasoning* to *neuro-symbolic reasoning*, while understanding how reasoning can be the enabler or the bottleneck to applications such as (visual and logical) *question answering*, *representation learning*, and more.

The Reading Group will also be a space for students to present their ongoing works and ask for feedback.

## The reading list
Feel free to vote (and add) papers you would like to discuss on this spreadsheet: 

https://docs.google.com/spreadsheets/d/1xCHOoZP93BU4hc55ZkL7aUqKB_7n1tUgTaNBDzVuiwQ/edit?usp=sharing

And please candidate yourself to present any work!


## This Repository
- Useful resources are archived in the corrsponding markdown files.
- Substaintial discussion can be hosted on [issues](https://git.ecdf.ed.ac.uk/reasoning-reading-group/readme/-/issues)
