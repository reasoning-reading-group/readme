# DreamCoder

The paper: https://arxiv.org/abs/2006.08381

A follow-up paper: https://arxiv.org/abs/2106.11053

The code: https://github.com/ellisk42/ec and https://github.com/CatherineWong/dreamcoder

Rim kindly walked through the work and led the discussion: [Reasoning-group-2022-Nov-17.pptx](/uploads/2d68fd71db20d9fb16633210a1ab2e4a/Reasoning-group-2022-Nov-17.pptx)



